import React, {Component} from 'react'

class RobotForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            name: '',
            type:'',
            mass : ''
        }
        this.handleChange = (evt) =>{
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.onAdd = () => {
            console.log(this.state)
            this.props.onAdd({
                name: this.state.name,
                mass: this.state.mass,
                type : this.state.type
            })
        }
    }
    render(){
        return <div>
            <input id="name" type="text" placeholder="name" name="name" onChange={this.handleChange}/>
            <input id="type" type="text" placeholder="type" name="type" onChange={this.handleChange}/>
            <input id="mass" type="number" placeholder="mass" name="mass" onChange={this.handleChange}/>

            <input type="button" value="add" onClick={this.onAdd}/>
        </div>
    }
}

export default RobotForm